﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Model;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace TimeRunners.Mobile.Forms.Views
{
    public delegate void TrackChanged(IEnumerable<TrackPoint> points);
    public class MapWithTrack : Map
    {
        public event TrackChanged TrackChangedEvent;

        public static readonly BindableProperty TrackPointsProperty =
            BindableProperty.Create("TrackPoints", typeof(IEnumerable<TrackPoint>), typeof(MapWithTrack), null, BindingMode.Default, null, TrackPointPropertyChanged);

        private static void TrackPointPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            
            if(oldValue is INotifyCollectionChanged)
                (oldValue as INotifyCollectionChanged).CollectionChanged -= (bindable as MapWithTrack).MapWithTrack_CollectionChanged;

            if (newValue is INotifyCollectionChanged)
                (newValue as INotifyCollectionChanged).CollectionChanged += (bindable as MapWithTrack).MapWithTrack_CollectionChanged;

            (bindable as MapWithTrack).OnTrackChangedEvent();
        }

        private void MapWithTrack_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnTrackChangedEvent();
        }

        public IEnumerable<TrackPoint> TrackPoints
        {
            get { return (IEnumerable<TrackPoint>)GetValue(TrackPointsProperty); }
            set { SetValue(TrackPointsProperty, value); }
        }

        protected virtual void OnTrackChangedEvent()
        {
            TrackChangedEvent?.Invoke(TrackPoints);
        }
    }
}
