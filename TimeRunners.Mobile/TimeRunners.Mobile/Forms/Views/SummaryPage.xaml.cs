﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Forms.ViewModels;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.Views
{
    public partial class SummaryPage : ContentPage
    {
        public SummaryPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var bindingContext = BindingContext as SummaryViewModel;

            bindingContext?.OnAppearing();
        }
    }
}
