﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TimeRunners.Mobile.Forms.ViewModels;
using TimeRunners.Mobile.Utils;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.Views
{
    public partial class RecordRunPage : ContentPage
    {
        public RecordRunPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var bindingContext = BindingContext as RecordRunViewModel;

            if (bindingContext != null)
                bindingContext.OnAppearing();

            MessagingCenter.Subscribe<object>(this, Messages.FinishRecord, async (sender) =>
            {
                Debug.WriteLine("Pop");
                await Navigation.PopAsync();
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<object>(this, Messages.FinishRecord);
        }
    }
}
