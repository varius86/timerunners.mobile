﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Forms.ViewModels;
using TimeRunners.Mobile.Model;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace TimeRunners.Mobile.Forms.Views
{
	public partial class RecordView : ContentView
	{
	    private RecordViewModel _recordViewModel;

	    public RecordView ()
		{
			InitializeComponent ();
            this.BindingContextChanged += RecordView_BindingContextChanged;

		}

	    private void RecordView_BindingContextChanged(object sender, EventArgs e)
	    {
	        if (_recordViewModel != null)
	            _recordViewModel.PropertyChanged -= RecordView_PropertyChanged;

	        _recordViewModel = BindingContext as RecordViewModel;

	        if (_recordViewModel != null)
	        {
	            _recordViewModel.PropertyChanged += RecordView_PropertyChanged;
	            if (_recordViewModel.LastTrackPoint != null)
	                Map.MoveToRegion(GetRegionFromLastPoint());
	        }
	    }

	    private void RecordView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "LastTrackPoint")
            {
                Map.MoveToRegion(GetRegionFromLastPoint());
            }
        }

	    private MapSpan GetRegionFromLastPoint()
	    {
	        return MapSpan.FromCenterAndRadius(new Position(_recordViewModel.LastTrackPoint.Latitude, _recordViewModel.LastTrackPoint.Longitude), Distance.FromKilometers(1));
	    }
	}
}
