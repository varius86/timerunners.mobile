﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Forms.ViewModels;
using TimeRunners.Mobile.Model;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.Views
{
    public partial class RecordsPage : MasterDetailPage
    {
        public RecordsPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var bindingContext = BindingContext as RecordListViewModel;
            if (bindingContext != null)
            {
                bindingContext.SelectedRecord = null;
                bindingContext?.OnAppearing();
            }

            MessagingCenter.Subscribe<object>(this, Messages.ContinueRun, async (sender) =>
            {
                await Navigation.PushAsync(new RecordRunPage());
                Navigation.RemovePage(this);
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            var bindingContext = BindingContext as RecordListViewModel;
            bindingContext?.OnDisappearing();

            MessagingCenter.Unsubscribe<object>(this, Messages.ContinueRun);
        }
    }
}
