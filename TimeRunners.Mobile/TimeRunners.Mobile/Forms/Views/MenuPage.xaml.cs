﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TimeRunners.Mobile.Forms.ViewModels;
using TimeRunners.Mobile.Utils;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.Views
{
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<object>(this, Messages.GoToLogOut, async (sender) =>
            {
                await Navigation.PushAsync(new LoginPage());
                Navigation.RemovePage(this);
            });

            MessagingCenter.Subscribe<object>(this, Messages.GoToRecordRun, async (sender) =>
            {
                await Navigation.PushAsync(new RecordRunPage());
            });

            MessagingCenter.Subscribe<object>(this, Messages.OpenRunList, async (sender) =>
            {
                await Navigation.PushAsync(new RecordsPage());
            });

            MessagingCenter.Subscribe<object>(this, Messages.OpenSummary, async (sender) =>
            {
                await Navigation.PushAsync(new SummaryPage());
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<object>(this, Messages.GoToLogOut);
            MessagingCenter.Unsubscribe<object>(this, Messages.GoToRecordRun);
            MessagingCenter.Unsubscribe<object>(this, Messages.OpenRunList);
            MessagingCenter.Unsubscribe<object>(this, Messages.OpenSummary);
        }
    }
}
