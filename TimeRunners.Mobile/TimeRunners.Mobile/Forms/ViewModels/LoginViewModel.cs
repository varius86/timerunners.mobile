﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TimeRunners.Mobile.Repositories;
using TimeRunners.Mobile.Services;
using TimeRunners.Mobile.Utils;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public bool CreateUserModeOn
        {
            get { return _createUserModeOn; }
            set
            {
                _createUserModeOn = value;
                OnPropertyChanged();
                OnPropertyChanged("ShowLoginButton");
            }
        }

        public bool ShowLoginButton => !CreateUserModeOn;

        private string _message;
        private bool _createUserModeOn;
        public Command LoginCommand { get; set; }
        public Command CreateUserCommand { get; set; }

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged();
            }
        }

        public LoginViewModel(IUserContext userContext)
        {
            Username = "User";
            Password = "Pass";

            IAuthService authService = AppContainer.Container.Resolve<IAuthService>();

            this.LoginCommand = new Command(async () =>
            {
                Message = "Logging in...";
                var result = await authService.Login(Username, Password);

                if (result.LoginState == LoginState.Authenticated)
                {
                    userContext.Current = result.AuthenticatedUser;
                    MessagingCenter.Send<object>(this, Messages.GoToMainMenu);
                }
                else
                {
                    Message = "Invalid credentials";
                }
            });

            this.CreateUserCommand = new Command(async () =>
            {
                if (!CreateUserModeOn)
                    CreateUserModeOn = true;
                else
                {
                    if (Password != ConfirmPassword)
                    {
                        Message = "Both passwords must be the same.";
                        return;
                    }

                    var createUserResult = await authService.CreateUser(Username, Password);
                    switch (createUserResult.State)
                    {
                        case CreateUserState.Created:
                            CreateUserModeOn = false;
                            Message = "User created!";
                            break;
                        case CreateUserState.AlreadyExists:
                            Message = "User with that name already exists";
                            break;
                        case CreateUserState.InvalidUsername:
                            Message = "Username mus only contain alphanumerical characters";
                            break;
                        case CreateUserState.InvalidPassword:
                            Message = "Password cannot be emtpy";
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            });
        }

        public void OnAppearing()
        {
            CreateUserModeOn = false;
            Message = "";
        }
    }
}
