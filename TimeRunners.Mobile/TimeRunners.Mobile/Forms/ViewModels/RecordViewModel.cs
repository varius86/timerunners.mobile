﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Services;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class RecordViewModel : ViewModelBase
    {
        private Record _record;
        private ObservableCollection<TrackPoint> _trackPoints;
        private ImageSource _photoSource;

        public RecordViewModel(Record record)
        {
            _record = record;
            if(record?.TrackPoints != null)
                _trackPoints = new ObservableCollection<TrackPoint>(_record.TrackPoints);

            if (!string.IsNullOrEmpty(_record.Photo))
                PhotoSource = ImageSource.FromFile(_record.Photo);
        }

        public double AverageSpeed => _record.AverageSpeed;

        public bool IsRecording
        {
            get { return _record.IsRecording; }
        }

        public bool IsPaused
        {
            get { return _record.IsPaused; }
        }

        public TimeSpan RunTime
        {
            get { return _record.RunTime; }
        }

        public RecordState State
        {
            get { return _record.State; }
        }

        public void Finish()
        {
            _record.Finish();
            OnStateChanged();
        }

        private void OnStateChanged()
        {
            OnPropertyChanged("IsPaused");
            OnPropertyChanged("IsRecording");
            OnPropertyChanged("State");
        }

        public DateTime StartTime => _record.StartTime;

        public void UpdateTime(TimeSpan elapsed)
        {
            _record.UpdateTime(elapsed);
            OnPropertyChanged("RunTime");
        }



        public TrackPoint LastTrackPoint => _record.LastTrackPoint;

        public double Distance => _record.Distance;

        public ObservableCollection<TrackPoint> TrackPoints => _trackPoints;

        public void UpdatePosition(DateTime timestamp, double latidute, double longitude)
        {
            _record.UpdatePosition(timestamp, latidute, longitude);

            foreach (var recordTrackPoint in _record.TrackPoints)
            {
                if(!_trackPoints.Contains(recordTrackPoint))
                    _trackPoints.Add(recordTrackPoint);
            }

            OnPropertyChanged("LastTrackPoint");
            OnPropertyChanged("Distance");
            OnPropertyChanged("TrackPoints");
            OnPropertyChanged("AverageSpeed");
        }

        public void TogglePause()
        {
            _record.TogglePause();
            OnStateChanged();
        }

        public Weather Weather => _record.Weather;

        public float Temperature => _record.Temperature;
        public string Photo
        {
            get { return _record.Photo; }
            set
            {
                _record.Photo = value;
                OnPropertyChanged();
            }
        }

        public ImageSource PhotoSource
        {
            get { return _photoSource; }
            set
            {
                _photoSource = value;
                OnPropertyChanged();
            }
        }

        public void UpdateWeather(WeatherInfo weatherInfo)
        {
            _record.Weather = weatherInfo.Weather;
            _record.Temperature = weatherInfo.Temperature;
            OnPropertyChanged("Weather");
            OnPropertyChanged("Temperature");
        }
    }
}
