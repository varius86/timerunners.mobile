namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class Messages
    {
        public const string GoToMainMenu = "GoToMainMenu";
        public const string FinishRecord = "FinishRecord";
        public const string GoToLogOut = "GoToLogOut";
        public const string GoToRecordRun = "GoToRecordRun";
        public const string OpenRunList = "OpenRunList";
        public const string OpenSummary = "OpenSummary";
        public const string ContinueRun = "ContinueRun";
    }
}