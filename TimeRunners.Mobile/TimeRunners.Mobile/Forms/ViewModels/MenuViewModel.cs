﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Forms.ViewModels;
using TimeRunners.Mobile.Utils;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class MenuViewModel
    {
        public MenuViewModel(IUserContext userContext)
        {
            this.LogoutCommand = new Command(() =>
            {
                userContext.Current = null;
                MessagingCenter.Send<object>(this, Messages.GoToLogOut);
            });

            this.RecordRunCommand = new Command(() =>
            {
                MessagingCenter.Send<object>(this, Messages.GoToRecordRun);
            });

            this.OpenRunListCommand = new Command(() =>
            {
                MessagingCenter.Send<object>(this, Messages.OpenRunList);
            });

            this.OpenSummaryCommand = new Command(() =>
            {
                MessagingCenter.Send<object>(this, Messages.OpenSummary);
            });
        }

        public Command OpenRunListCommand { get; set; }

        public Command LogoutCommand { get; set; }

        public Command RecordRunCommand { get; set; }

        public Command OpenSummaryCommand { get; set; }
    }
}
