using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public virtual event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void OnAppearing()
        {
            
        }

        public virtual void OnDisappearing()
        {

        }
    }
}