﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TimeRunners.Mobile.Utils;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            
        }

        public LoginViewModel Login => AppContainer.Container.Resolve<LoginViewModel>();
        public MenuViewModel Menu => AppContainer.Container.Resolve<MenuViewModel>();
        public RecordRunViewModel RecordRun => AppContainer.Container.Resolve<RecordRunViewModel>();
        public RecordListViewModel RecordList => AppContainer.Container.Resolve<RecordListViewModel>();
        public SummaryViewModel Summary => AppContainer.Container.Resolve<SummaryViewModel>();

        public static void Cleanup()
        {
        }
    }
}
