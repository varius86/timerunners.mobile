﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Media;
using Plugin.Media.Abstractions;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Repositories;
using TimeRunners.Mobile.Services;
using TimeRunners.Mobile.Utils;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class RecordRunViewModel : ViewModelBase
    {
        private readonly TimeSpan _timerInterval;
        private readonly IUserContext _userContext;
        private readonly IUserRepository _userRepository;
        private IWeatherService _weatherService;
        private WeatherInfo _weatherInfo;
        private ImageSource _photo;

        public RecordRunViewModel(IUserContext userContext, IUserRepository userRepository, IWeatherService weatherService)
        {
            _userContext = userContext;
            _userRepository = userRepository;
            _weatherService = weatherService;

            _timerInterval = TimeSpan.FromMilliseconds(1000);

            SetupCommands();
            SetupLocationUpdate();
        }

        private async Task SetupStartingPoint()
        {
            var position = await CrossGeolocator.Current.GetPositionAsync();
            Record.UpdatePosition(DateTime.Now, position.Latitude, position.Longitude);
            SaveChanges();
            _weatherInfo = await _weatherService.GetWeather(Record.LastTrackPoint.Latitude,
                Record.LastTrackPoint.Longitude);
            Record.UpdateWeather(_weatherInfo);
        }

        private async void RecordRun()
        {
            Record = new RecordViewModel(_userContext.Current.RecordRun());
            OnPropertyChanged("Record");
            OnPropertyChanged("PauseButtonText");
            SaveChanges();
            StartTimers();
            if (Record.LastTrackPoint == null)
            {
                await SetupStartingPoint();
            }
            StartGeolocation();
        }

        private void StartTimers()
        {
            var record = Record;
            Device.StartTimer(_timerInterval, () =>
            {
                record.UpdateTime(_timerInterval);
                return record.IsRecording;
            });

            Device.StartTimer(TimeSpan.FromSeconds(10), () =>
            {
                SaveChanges();
                return record.IsRecording;
            });

            StartGeolocation();
        }

        private static void StartGeolocation()
        {
            CrossGeolocator.Current.StartListeningAsync(1, 1, false);
        }

        private void SetupLocationUpdate()
        {
            CrossGeolocator.Current.DesiredAccuracy = 50;
            CrossGeolocator.Current.PositionChanged += async (sender, e) =>
            {
                Record?.UpdatePosition(DateTime.Now, e.Position.Latitude, e.Position.Longitude);
                _weatherInfo = await _weatherService.GetWeather(e.Position.Latitude,
                    e.Position.Longitude);
                Record?.UpdateWeather(_weatherInfo);
            };
        }

        private void SaveChanges()
        {
            if (_userContext.Current != null)
                _userRepository.SaveUser(_userContext.Current);
        }

        private void SetupCommands()
        {
            PauseCommand = new Command(() =>
            {
                Record.TogglePause();
                if (Record.IsRecording)
                {
                    StartTimers();
                    StartGeolocation();
                }
                else
                    StopGeolocation();
                OnPropertyChanged("PauseButtonText");
                SaveChanges();
            });

            FinishCommand = new Command(() =>
            {
                Record.Finish();
                StopGeolocation();
                SaveChanges();
                MessagingCenter.Send<object>(this, Messages.FinishRecord);
            });

            TakePhotoCommand = new Command(async () =>
            {
                await CrossMedia.Current.Initialize();

                var photoName = "Run_" + DateTime.Now.ToString();
                var file =
                    await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        SaveToAlbum = true,
                        Name = photoName
                    });

                Record.Photo = file.Path;

                Record.PhotoSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            });
        }

        private static void StopGeolocation()
        {
            CrossGeolocator.Current.StopListeningAsync();
        }

        public ICommand PauseCommand { get; set; }
        public ICommand FinishCommand { get; set; }

        public string PauseButtonText
        {
            get
            {
                var isRecording = Record?.IsRecording;
                if (!isRecording.HasValue || isRecording.Value)
                    return "Pause";
                return "Paused (Continue)";
            }
        }

        public RecordViewModel Record { get; set; }

        public Command TakePhotoCommand { get; set; }

        public override void OnAppearing()
        {
            RecordRun();
        }
    }
}