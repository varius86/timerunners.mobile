﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using TimeRunners.Mobile.Forms.Views;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Repositories;
using TimeRunners.Mobile.Utils;
using Xamarin.Forms;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class RecordListViewModel : ViewModelBase
    {
        private readonly IUserContext _userContext;
        private readonly IUserRepository _userRepository;
        private DateTime _fromDate;
        private Record _selectedRecord;
        private DateTime _toDate;
        private ObservableCollection<Record> _records;

        public RecordListViewModel(IUserContext userContext, IUserRepository userRepository)
        {
            _userContext = userContext;
            _userRepository = userRepository;

            FromDate = DateTime.Now.AddDays(-7);
            ToDate = DateTime.Now;

            DeleteCommand = new Command(() =>
            {
                var selectedItem = Records.Remove(SelectedRecord);
                _userContext.Current.Records.Remove(SelectedRecord);
                _userRepository.SaveUser(_userContext.Current);
                SelectedRecord = null;
                Notify();
            });

            ContinueCommand = new Command(() =>
            {
                _userContext.Current.ContinueRun(SelectedRecord);
                _userRepository.SaveUser(_userContext.Current);
                MessagingCenter.Send<object>(this, Messages.ContinueRun);
            });
        }

        public Command DeleteCommand { get; set; }
        public Command ContinueCommand { get; set; }

        public DateTime FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                FilterRecords();
                OnPropertyChanged();
            }
        }

        public DateTime ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                FilterRecords();
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Record> Records
        {
            get { return _records; }
            set
            {
                _records = value; 
                OnPropertyChanged();
                OnPropertyChanged("NoRecords");
            }
        }

        public Record SelectedRecord
        {
            get { return _selectedRecord; }
            set
            {
                _selectedRecord = value;
                Notify();
            }
        }

        public bool IsPresented => SelectedRecord == null;

        public bool NoRecords => Records == null || Records.Count == 0;

        public RecordViewModel SelectedRecordViewModel
        {
            get
            {
                if (SelectedRecord == null)
                    return null;
                return new RecordViewModel(SelectedRecord);
            }
        }

        private void Notify()
        {
            OnPropertyChanged("SelectedRecord");
            OnPropertyChanged("SelectedRecordViewModel");
            OnPropertyChanged("IsPresented");
            OnPropertyChanged("Records");
            OnPropertyChanged("NoRecords");
        }

        public override void OnAppearing()
        {
            FilterRecords();
            Notify();
        }

        private void FilterRecords()
        {
            Records =
                new ObservableCollection<Record>(
                    _userContext.Current.Records.Where(x => x.StartTime >= FromDate && x.StartTime.Date <= ToDate));
        }
    }
}