﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Utils;

namespace TimeRunners.Mobile.Forms.ViewModels
{
    public class SummaryViewModel : ViewModelBase
    {
        private IUserContext _userContext;

        public SummaryViewModel(IUserContext userContext)
        {
            _userContext = userContext;
        }

        public IEnumerable<SummaryItem> SummaryItems
        {
            get
            {
                return _userContext.Current.Records.GroupBy(x => x.StartTime.Date.AddDays(-(int)x.StartTime.DayOfWeek)).Select(
                    x => new SummaryItem
                    {
                        AverageSpeed = x.Sum(a => a.AverageSpeed) / 7,
                        Distance = x.Sum(a => a.Distance),
                        StartTime = x.Key
                    });
            }
        }

        public bool NoRecords => !SummaryItems.Any();

        public override void OnAppearing()
        {
            base.OnAppearing();
            OnPropertyChanged("SummaryItems");
            OnPropertyChanged("NoRecords");
        }
    }

    public class SummaryItem
    {
        public double AverageSpeed { get; set; }
        public double AverageDistance => Distance / 7;
        public double Distance { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime => StartTime.AddDays(7);
    }
}
