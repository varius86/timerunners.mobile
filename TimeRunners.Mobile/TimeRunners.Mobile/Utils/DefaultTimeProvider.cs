using System;

namespace TimeRunners.Mobile.Utils
{
    public class DefaultTimeProvider : TimeProvider
    {
        public static TimeProvider Instance = new DefaultTimeProvider();

        public override DateTime Now => DateTime.Now;
    }
}