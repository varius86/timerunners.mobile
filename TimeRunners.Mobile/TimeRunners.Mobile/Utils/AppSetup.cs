﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Repositories;
using TimeRunners.Mobile.Services;

namespace TimeRunners.Mobile.Utils
{
    public class AppSetup
    {
        public IContainer CreateContainer()
        {
            var containerBuilder = new ContainerBuilder();
            RegisterDependencies(containerBuilder);
            return containerBuilder.Build();
        }

        protected virtual void RegisterDependencies(ContainerBuilder cb)
        {
            var assembly = typeof(AuthService).GetTypeInfo().Assembly;

            cb.RegisterAssemblyTypes(assembly)
                   .AsImplementedInterfaces();

            cb.RegisterAssemblyTypes(assembly)
                .Where(x => x.Name.EndsWith("ViewModel"))
                .SingleInstance();

            cb.RegisterType<UserContext>().As<IUserContext>().SingleInstance();
        }
    }
}
