﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Model;

namespace TimeRunners.Mobile.Utils
{
    public interface IUserContext
    {
        User Current { get; set; }
    }

    public class UserContext : IUserContext
    {
        public User Current { get; set; }
    }
}
