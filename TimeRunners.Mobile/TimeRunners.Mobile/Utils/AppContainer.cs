﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TimeRunners.Mobile.Model;

namespace TimeRunners.Mobile.Utils
{
    public static class AppContainer
    {
        public static IContainer Container { get; set; }
    }
}
