using System.Text;
using Org.BouncyCastle.Crypto.Digests;

namespace TimeRunners.Mobile.Services
{
    public class CryptoService : ICryptoService
    {
        public CryptoService()
        {
        }

        public string CalculateHash(string password, string seed)
        {
            Sha256Digest digester = new Sha256Digest();
            byte[] retValue = new byte[digester.GetDigestSize()];
            var input = Encoding.UTF8.GetBytes(password + seed);
            digester.BlockUpdate(input, 0, input.Length);
            digester.DoFinal(retValue, 0);
            return Encoding.UTF8.GetString(retValue, 0, retValue.Length);
        }
    }
}