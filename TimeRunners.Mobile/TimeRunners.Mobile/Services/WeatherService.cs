﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TimeRunners.Mobile.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly string _apiKey = "6c37e33aa10d25c0067dd6781aeb4c3b";
        private readonly string _apiUrl;

        private double? _cachedLatitude;
        private double? _cachedLongitude;
        private DateTime? _cachedTimestamp;
        private WeatherInfo _cachedWeatherInfo;

        public WeatherService()
        {
            _apiUrl = "http://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&units=metric&APPID=" + _apiKey;
        }

        public async Task<WeatherInfo> GetWeather(double latitude, double longitude)
        {
            if(IsCacheValid(latitude, longitude))
                return await Task.FromResult(_cachedWeatherInfo);

            var client = new HttpClient();
            var requestUri = string.Format(_apiUrl, latitude, longitude);

            var response = await client.GetAsync(requestUri);

            var jsonReponse = await response.Content.ReadAsStringAsync();

            var jObject = JObject.Parse(jsonReponse);

            var icon = jObject["weather"][0]["icon"].ToString();
            var temperature = float.Parse(jObject["main"]["temp"].ToString());

            var weatherInfo = WeatherInfo.FromIcon(icon, temperature);

            Cache(weatherInfo, latitude, longitude);

            return await Task.FromResult(weatherInfo);
        }

        private void Cache(WeatherInfo weatherInfo, double latitude, double longitude)
        {
            _cachedTimestamp = DateTime.Now;
            _cachedLatitude = latitude;
            _cachedLongitude = longitude;
            _cachedWeatherInfo = weatherInfo;
        }

        private bool IsCacheValid(double latitude, double longitude)
        {
            return _cachedWeatherInfo!=null && _cachedTimestamp.HasValue && _cachedLatitude.HasValue && _cachedLongitude.HasValue
                && _cachedTimestamp.Value >= DateTime.Now.AddHours(-1) &&
                Math.Abs(_cachedLatitude.Value - latitude) < 0.1 && Math.Abs(_cachedLongitude.Value - longitude) < 0.1;
        }
    }

    public class WeatherInfo
    {
        public Weather Weather { get; set; }
        public float Temperature { get; set; }

        public static WeatherInfo FromIcon(string icon, float temperature)
        {
            return new WeatherInfo {Weather = _weathers[icon.Remove(icon.Length-1)], Temperature = temperature};
        }

        private static readonly Dictionary<string, Weather> _weathers = new Dictionary<string, Weather>
        {
            { "01", Weather.ClearSky},
            { "02", Weather.FewClouds},
            { "03", Weather.ScatteredClouds},
            { "04", Weather.BrokenClouds},
            { "09", Weather.ShowerRain},
            { "10", Weather.Rain},
            { "11", Weather.Thunderstorm},
            { "13", Weather.Snow},
            { "50", Weather.Mist},
        };
    }

    public enum Weather
    {
        ClearSky,
        FewClouds,
        ScatteredClouds,
        BrokenClouds,
        ShowerRain,
        Rain,
        Thunderstorm,
        Snow,
        Mist
    }
}
