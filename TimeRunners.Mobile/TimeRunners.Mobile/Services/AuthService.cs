﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.Digests;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Repositories;

namespace TimeRunners.Mobile.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly ICryptoService _cryptoService;

        public AuthService(IUserRepository userRepository, ICryptoService cryptoService)
        {
            _userRepository = userRepository;
            _cryptoService = cryptoService;
        }

        public async Task<LoginResult> Login(string username, string password)
        {
            var user = await _userRepository.GetUser(username);

            if (user != null)
            {
                var passwordHash = _cryptoService.CalculateHash(password, user.Seed);
                if(user.Password == passwordHash)
                    return new LoginResult { AuthenticatedUser = user, LoginState = LoginState.Authenticated};
            }

            return new LoginResult {LoginState = LoginState.InvalidCredentials};
        }


        public async Task<CreateUserResult> CreateUser(string username, string password)
        {
            if(string.IsNullOrEmpty(username) || !username.All(char.IsLetterOrDigit))
                return CreateUserResult.InvalidUsername();

            if(string.IsNullOrEmpty(password))
                return CreateUserResult.InvalidPassword();

            var previousUser = await _userRepository.GetUser(username);
            if (previousUser != null)
                return CreateUserResult.AlreadyExists();

            var user = new User();
            user.Username = username;
            user.Seed = Guid.NewGuid().ToString();
            user.Password = _cryptoService.CalculateHash(password, user.Seed);

            await _userRepository.SaveUser(user);

            return CreateUserResult.Created(user);
        }
    }

    public class LoginResult
    {
        public LoginState LoginState;
        public User AuthenticatedUser;
    }

    public enum LoginState
    {
        Authenticated,
        InvalidCredentials
    }

    public class CreateUserResult
    {
        public CreateUserState State;
        public User CreatedUser;

        private CreateUserResult()
        {
            
        }

        public static CreateUserResult Created(User user)
        {
            return new CreateUserResult { CreatedUser = user, State = CreateUserState.Created };
        }
        public static CreateUserResult AlreadyExists()
        {
            return new CreateUserResult { State = CreateUserState.AlreadyExists };
        }
        public static CreateUserResult InvalidUsername()
        {
            return new CreateUserResult { State = CreateUserState.InvalidUsername };
        }
        public static CreateUserResult InvalidPassword()
        {
            return new CreateUserResult { State = CreateUserState.InvalidPassword };
        }
    }

    public enum CreateUserState
    {
        Created,
        AlreadyExists,
        InvalidUsername,
        InvalidPassword
    }
}
