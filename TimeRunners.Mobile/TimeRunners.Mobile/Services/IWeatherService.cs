using System.Threading.Tasks;

namespace TimeRunners.Mobile.Services
{
    public interface IWeatherService
    {
        Task<WeatherInfo> GetWeather(double latitude, double longitude);
    }
}