namespace TimeRunners.Mobile.Services
{
    public interface ICryptoService
    {
        string CalculateHash(string password, string seed);
    }
}