using System.Threading.Tasks;

namespace TimeRunners.Mobile.Services
{
    public interface IAuthService
    {
        Task<LoginResult> Login(string user, string pass);
        Task<CreateUserResult> CreateUser(string username, string password);
    }

}