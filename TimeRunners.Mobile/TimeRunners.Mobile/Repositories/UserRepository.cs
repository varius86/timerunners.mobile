﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using TimeRunners.Mobile.Database;
using TimeRunners.Mobile.Model;
using SQLiteNetExtensions.Extensions;
using SQLiteNetExtensionsAsync.Extensions;

namespace TimeRunners.Mobile.Repositories
{
    public class UserRepository : IUserRepository
    {
        private SQLiteAsyncConnection _database;

        public UserRepository(IFileHelper fileHelper)
        {
            _database = new SQLiteAsyncConnection(fileHelper.GetLocalFilePath("Timerunners.db3"));
            _database.CreateTableAsync<User>().Wait();
            _database.CreateTableAsync<Record>().Wait();
            _database.CreateTableAsync<TrackPoint>().Wait();
        }

        public async Task<User> GetUser(string username)
        {
            var user = await _database.Table<User>().Where(i => i.Username == username).FirstOrDefaultAsync();
            if (user != null)
                await _database.GetChildrenAsync(user, true);
            return user;
        }

        public async Task SaveUser(User user)
        {
            await _database.InsertOrReplaceWithChildrenAsync(user, true);
        }
    }
}
