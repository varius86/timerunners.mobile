using System.Threading.Tasks;
using TimeRunners.Mobile.Model;

namespace TimeRunners.Mobile.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetUser(string username);
        Task SaveUser(User user);
    }
}