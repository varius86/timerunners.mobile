using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using SQLiteNetExtensions.Attributes;
using TimeRunners.Mobile.Forms.ViewModels;
using TimeRunners.Mobile.Services;
using TimeRunners.Mobile.Utils;

namespace TimeRunners.Mobile.Model
{
    public class Record
    {
        public Record()
        {
            StartTime = TimeProvider.Current.Now;
            State = RecordState.Recording;
            TrackPoints = new List<TrackPoint>();
        }
        public Record(User user) : this()
        {
            User = user;
        }

        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }

        public DateTime StartTime { get; set; }

        [ManyToOne]
        public User User { get; set; }

        [ForeignKey(typeof(User))]
        public int UserId { get; set; }
        public string Photo { get; set; }

        public bool IsRecording => State == RecordState.Recording;
        public bool IsPaused => State == RecordState.Paused;

        public TimeSpan RunTime { get; private set; }

        public RecordState State { get; private set; }

        public void Finish()
        {
            State = RecordState.Finished;
        }

        public void UpdateTime(TimeSpan elapsed)
        {
            if (IsRecording)
                RunTime += elapsed;
        }

        public void Pause()
        {
            State = RecordState.Paused;
        }

        public void Continue()
        {
            State = RecordState.Recording;
        }

        public void UpdatePosition(DateTime timestamp, double latidute, double longitude)
        {
            if (IsRecording)
                AddPoint(timestamp, latidute, longitude);
        }

        private List<TrackPoint> _trackPoints;

        public TrackPoint LastTrackPoint => _trackPoints.LastOrDefault();

        public double Distance { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<TrackPoint> TrackPoints
        {
            get { return _trackPoints; }
            set { _trackPoints = value; }
        }

        private void AddPoint(DateTime timestamp, double latidute, double longitude)
        {
            var currentPoint = new TrackPoint(timestamp, latidute, longitude);
            if (LastTrackPoint != null)
                Distance += LastTrackPoint.DistanceTo(currentPoint);
            CalculateAverageSpeed();
            _trackPoints.Add(currentPoint);
        }

        private void CalculateAverageSpeed()
        {
            if (Distance > 0)
            {
                AverageSpeed = Distance / RunTime.TotalHours;
            }
        }

        public double AverageSpeed { get; set; }
        public Weather Weather { get; set; }
        public float Temperature { get; set; }

        public void TogglePause()
        {
            if (IsPaused)
                Continue();
            else
                Pause();
        }
    }

    public enum RecordState
    {
        Recording = 0,
        Paused = 1,
        Finished = 2
    }
}