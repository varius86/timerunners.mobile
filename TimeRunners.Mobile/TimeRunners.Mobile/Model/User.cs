﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Crypto.Digests;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace TimeRunners.Mobile.Model
{
    public class User
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }

        public User()
        {
            Records = new List<Record>();
        }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Record> Records { get; set; }

        public string Username { get; set; }

        public string Seed { get; set; }
        public string Password { get; set; }

        public Record RecordRun()
        {
            if (RunInProgress != null)
                return RunInProgress;
            var record = new Record(this);
            Records.Add(record);
            return record;
        }
        
        public void ContinueRun(Record record)
        {
            RunInProgress?.Finish();
            record.Continue();
        }

        public Record RunInProgress => Records.FirstOrDefault(x => x.State != RecordState.Finished);
    }
}