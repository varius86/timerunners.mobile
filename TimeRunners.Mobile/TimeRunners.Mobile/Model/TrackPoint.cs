using System;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace TimeRunners.Mobile.Model
{
    public class TrackPoint
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }

        [ManyToOne]
        public Record Record { get; set; }

        [ForeignKey(typeof(Record))]
        public int RecordId { get; set; }

        public TrackPoint()
        {
            
        }

        public TrackPoint(DateTime timestamp, double latitude, double longitude)
        {
            Timestamp = timestamp;
            Latitude = latitude;
            Longitude = longitude;
        }

        public DateTime Timestamp { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}