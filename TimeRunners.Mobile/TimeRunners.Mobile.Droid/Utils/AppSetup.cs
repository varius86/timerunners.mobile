using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Autofac;
using TimeRunners.Mobile.Database;

namespace TimeRunners.Mobile.Droid.Utils
{
    public class AppSetup : Mobile.Utils.AppSetup
    {
        protected override void RegisterDependencies(ContainerBuilder cb)
        {
            base.RegisterDependencies(cb);
            cb.RegisterType<FileHelper>().As<IFileHelper>();
        }
    }
}