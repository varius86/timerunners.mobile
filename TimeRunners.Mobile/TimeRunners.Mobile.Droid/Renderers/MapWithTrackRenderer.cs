using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.Geolocator.Abstractions;
using TimeRunners.Mobile.Droid.Renderers;
using TimeRunners.Mobile.Forms.Views;
using TimeRunners.Mobile.Model;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using Xamarin.Forms.Platform.Android;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(MapWithTrack), typeof(MapWithTrackRenderer))]
namespace TimeRunners.Mobile.Droid.Renderers
{
    class MapWithTrackRenderer : MapRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {

            if (e.OldElement != null)
            {
                // Unsubscribe
            }

            if (e.NewElement != null)
            {
                var formsMap = (MapWithTrack) e.NewElement;
                formsMap.TrackChangedEvent += FormsMap_TrackChangedEvent;
            }

            base.OnElementChanged(e);
        }

        private void FormsMap_TrackChangedEvent(IEnumerable<TrackPoint> points)
        {
            if (NativeMap != null)
            {
                NativeMap.Clear();

                if (points != null && points.Any())
                {
                    var polylineOptions = new PolylineOptions();
                    polylineOptions.InvokeColor(0x66FF0000);

                    foreach (var position in points)
                    {
                        polylineOptions.Add(new LatLng(position.Latitude, position.Longitude));
                    }

                    NativeMap.AddPolyline(polylineOptions);
                    NativeMap.AddMarker(new MarkerOptions().SetPosition(polylineOptions.Points[0]).SetTitle("Start"));
                }
            }
        }
    }
}