﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreLocation;
using MapKit;
using TimeRunners.Mobile.Forms.Views;
using TimeRunners.Mobile.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;
using System.Linq;
using UIKit;

[assembly: ExportRenderer(typeof(MapWithTrack), typeof(MapWithTrackRenderer))]
namespace TimeRunners.Mobile.iOS.Renderers
{
    public class MapWithTrackRenderer : MapRenderer
    {
        MKPolylineRenderer polylineRenderer;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                nativeMap.OverlayRenderer = null;
            }

            if (e.NewElement != null)
            {
                var formsMap = (MapWithTrack)e.NewElement;
                formsMap.TrackChangedEvent += FormsMap_TrackChangedEvent;
                var nativeMap = Control as MKMapView;
                nativeMap.OverlayRenderer = GetOverlayRenderer;
            }
        }

        private void FormsMap_TrackChangedEvent(IEnumerable<Model.TrackPoint> points)
        {
            var nativeMap = this.Control as MKMapView;

            foreach (var nativeMapOverlay in nativeMap.Overlays)
            {
                nativeMap.RemoveOverlay(nativeMapOverlay);
            }

            if (points != null && points.Any())
            {
                CLLocationCoordinate2D[] coords = new CLLocationCoordinate2D[points.Count()];

                int index = 0;
                foreach (var position in points)
                {
                    coords[index] = new CLLocationCoordinate2D(position.Latitude, position.Longitude);
                    index++;
                }

                var routeOverlay = MKPolyline.FromCoordinates(coords);
                nativeMap.AddOverlay(routeOverlay);
            }
        }

        MKOverlayRenderer GetOverlayRenderer(MKMapView mapView, IMKOverlay overlay)
        {
            if (polylineRenderer == null)
            {
                polylineRenderer = new MKPolylineRenderer(overlay as MKPolyline);
                polylineRenderer.FillColor = UIColor.Blue;
                polylineRenderer.StrokeColor = UIColor.Red;
                polylineRenderer.LineWidth = 3;
                polylineRenderer.Alpha = 0.4f;
            }
            return polylineRenderer;
        }
    }
}
