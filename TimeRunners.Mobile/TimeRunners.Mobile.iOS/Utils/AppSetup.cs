﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using TimeRunners.Mobile.Database;

namespace TimeRunners.Mobile.iOS.Utils
{
    public class AppSetup : Mobile.Utils.AppSetup
    {
        protected override void RegisterDependencies(ContainerBuilder cb)
        {
            base.RegisterDependencies(cb);
            cb.RegisterType<FileHelper>().As<IFileHelper>();
        }
    }
}
