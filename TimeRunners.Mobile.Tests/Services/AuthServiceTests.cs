
using System;
using System.Net;
using System.Threading.Tasks;
using PCLMock;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Repositories;
using TimeRunners.Mobile.Repositories.Mocks;
using TimeRunners.Mobile.Services;
using TimeRunners.Mobile.Services.Mocks;
using Xunit;

namespace TimeRunners.Mobile.Tests.Services
{
    public class AuthServiceTests
    {
        private AuthService _service;
        private readonly UserRepositoryMock _userRepositoryMock;
        private string _correctUsername = "user";
        private string _correctPassword = "pass";
        private string _passwordHash = "someHash";

        private string _newUsername = "userExist";
        private string _newPassword = "passWitHash";
        private string _newPasswordHash = "passWitHash";

        private CryptoServiceMock _cryptoServiceMock;


        public AuthServiceTests()
        {
            _userRepositoryMock = new UserRepositoryMock(MockBehavior.Loose);
            _cryptoServiceMock = new CryptoServiceMock(MockBehavior.Loose);
            _service = new AuthService(_userRepositoryMock, _cryptoServiceMock);

            _userRepositoryMock.When(x => x.GetUser(It.IsAny<string>())).Return(Task.FromResult((User)null));
            _userRepositoryMock.When(x => x.GetUser(_correctUsername)).Return(Task.FromResult(new User { Password = _passwordHash }));

            _userRepositoryMock.When(x => x.SaveUser(It.IsAny<User>())).Return(Task.FromResult(0));

            _cryptoServiceMock.When(x => x.CalculateHash(_correctPassword, It.IsAny<string>())).Return(_passwordHash);
            _cryptoServiceMock.When(x => x.CalculateHash(_newPassword, It.IsAny<string>())).Return(_newPasswordHash);
        }

        [Fact]
        public async void AuthenticationSucceedsWhenCorrectPasswordIsUsed()
        {
            LoginResult result = await _service.Login(_correctUsername, _correctPassword);

            Assert.NotNull(result);
            Assert.Equal(LoginState.Authenticated, result.LoginState);
            Assert.NotNull(result.AuthenticatedUser);
        }

        [Fact]
        public async void AuthenticationFailsWhenIncorrectPasswordIsUsed()
        {
            LoginResult result = await _service.Login(_correctUsername, "somepass");

            Assert.NotNull(result);
            Assert.Equal(LoginState.InvalidCredentials, result.LoginState);
            Assert.Null(result.AuthenticatedUser);
        }

        [Fact]
        public async void AuthenticationFailsWhenIncorrectUserIsUsed()
        {
            LoginResult result = await _service.Login("someuser", "somepass");

            Assert.NotNull(result);
            Assert.Equal(LoginState.InvalidCredentials, result.LoginState);
            Assert.Null(result.AuthenticatedUser);
        }

        [Fact]
        public async void CreateUser()
        {
            CreateUserResult result = await _service.CreateUser("newUser", "newPassword");

            Assert.NotNull(result);
            Assert.Equal(CreateUserState.Created, result.State);
            Assert.NotNull(result.CreatedUser);
        }

        [Fact]
        public async void HashesUserPassword()
        {
            CreateUserResult result = await _service.CreateUser(_newUsername, _newPassword);

            Assert.NotNull(result.CreatedUser.Password);
            Assert.NotNull(result.CreatedUser.Seed);
            Assert.Equal(_newPasswordHash, result.CreatedUser.Password);
        }

        [Fact]
        public async void CreateUserFailsWhenUserExists()
        {
            CreateUserResult result = await _service.CreateUser(_correctUsername, _correctPassword);

            Assert.NotNull(result);
            Assert.Equal(CreateUserState.AlreadyExists, result.State);
            Assert.Null(result.CreatedUser);
        }

        [Fact]
        public async void CreateUserFailsWhenEmptyPassword()
        {
            CreateUserResult result = await _service.CreateUser(_correctUsername, String.Empty);

            Assert.NotNull(result);
            Assert.Equal(CreateUserState.InvalidPassword, result.State);
            Assert.Null(result.CreatedUser);
        }

        [Fact]
        public async void CreateUserFailsWhenEmptyUser()
        {
            CreateUserResult result = await _service.CreateUser(String.Empty, _correctPassword);

            Assert.NotNull(result);
            Assert.Equal(CreateUserState.InvalidUsername, result.State);
            Assert.Null(result.CreatedUser);
        }


        [Fact]
        public async void CreateUserFailsWhenWrongUsername()
        {
            CreateUserResult result = await _service.CreateUser("usr % &^", _correctPassword);

            Assert.NotNull(result);
            Assert.Equal(CreateUserState.InvalidUsername, result.State);
            Assert.Null(result.CreatedUser);
        }
    }
}