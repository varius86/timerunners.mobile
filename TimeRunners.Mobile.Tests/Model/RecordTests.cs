﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Tests.Utils;
using TimeRunners.Mobile.Utils;
using Xunit;
using Record = TimeRunners.Mobile.Model.Record;

namespace TimeRunners.Mobile.Tests.Model
{
    public class RecordTests
    {
        private ManualTimeProvider _manualTimeProvider;
        private User _user;

        public RecordTests()
        {
            _manualTimeProvider = new ManualTimeProvider();
            TimeProvider.Current = _manualTimeProvider;
            _user = new User();
        }

        [Fact]
        public void SetsStartTime()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            Assert.Equal(startTime, record.StartTime);
        }

        [Fact]
        public void UpdateTime()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            record.UpdateTime(TimeSpan.FromSeconds(15));
            record.UpdateTime(TimeSpan.FromSeconds(10));

            Assert.Equal(TimeSpan.FromSeconds(25), record.RunTime);
        }

        [Fact]
        public void PauseChangesState()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            record.Pause();

            Assert.Equal(RecordState.Paused, record.State);
        }

        [Fact]
        public void PausedRecordDoesNotUpdateRunTime()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            record.UpdateTime(TimeSpan.FromSeconds(15));
            record.Pause();
            record.UpdateTime(TimeSpan.FromSeconds(10));

            Assert.Equal(TimeSpan.FromSeconds(15), record.RunTime);
        }

        [Fact]
        public void FinishedRecordDoesNotUpdateRunTime()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            record.UpdateTime(TimeSpan.FromSeconds(15));
            record.Finish();
            record.UpdateTime(TimeSpan.FromSeconds(10));

            Assert.Equal(TimeSpan.FromSeconds(15), record.RunTime);
        }

        [Fact]
        public void FinishedRecordDoesContinue()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            record.Finish();
            record.Continue();

            Assert.Equal(RecordState.Recording, record.State);
        }

        [Fact]
        public void ContinueChangesState()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            record.Continue();

            Assert.Equal(RecordState.Recording, record.State);
        }

        [Fact]
        public void FinishChangesState()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            record.Finish();

            Assert.Equal(RecordState.Finished, record.State);
        }

        [Fact]
        public void UpdatesPosition()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            var latidute = 1.0;
            var longitude = 1.0;
            var timestamp = DateTime.Now;
            record.UpdatePosition(timestamp, latidute, longitude);

            Assert.Equal(1, record.TrackPoints.Count());

            var trackPoint = record.LastTrackPoint;
            Assert.Equal(timestamp, trackPoint.Timestamp);
            Assert.Equal(latidute, trackPoint.Latitude);
            Assert.Equal(longitude, trackPoint.Longitude);
        }

        [Fact]
        public void CalculatesAverageSpeed()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            var latidute = 1.0;
            var longitude = 1.0;
            var timestamp = DateTime.Now;
            record.UpdatePosition(timestamp, latidute, longitude);
            record.UpdatePosition(timestamp, latidute * 2, longitude * 2);

            Assert.Equal(2, record.TrackPoints.Count());

            Assert.True(record.AverageSpeed > 0);
        }


        [Fact]
        public void UpdatesDistance()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);

            var latidute = 1.0;
            var longitude = 1.0;
            var timestamp = DateTime.Now;
            record.UpdatePosition(timestamp, latidute, longitude);
            record.UpdatePosition(timestamp, latidute * 2, longitude * 2);

            Assert.Equal(2, record.TrackPoints.Count());

            Assert.True(record.Distance > 0);
        }

        [Fact]
        public void DoesNotUpdatePositionWhenPaused()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);
            record.Pause();
            var latidute = 1.0;
            var longitude = 1.0;
            var timestamp = DateTime.Now;
            record.UpdatePosition(timestamp, latidute, longitude);

            Assert.Equal(0, record.TrackPoints.Count());
        }

        [Fact]
        public void DoesNotUpdatePositionWhenFinished()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);
            record.Finish();
            var latidute = 1.0;
            var longitude = 1.0;
            var timestamp = DateTime.Now;
            record.UpdatePosition(timestamp, latidute, longitude);

            Assert.Equal(0, record.TrackPoints.Count());
        }

        [Fact]
        public void TogglePauseFromRunningToPause()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);
            record.TogglePause();
            Assert.Equal(RecordState.Paused, record.State);
        }

        [Fact]
        public void TogglePauseFromPauseToRecording()
        {
            var startTime = DateTime.Now;
            _manualTimeProvider.SetTime(startTime);

            var record = new Record(_user);
            record.Pause();
            record.TogglePause();
            Assert.Equal(RecordState.Recording, record.State);
        }
    }
}
