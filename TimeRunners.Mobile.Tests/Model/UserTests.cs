﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Model;
using TimeRunners.Mobile.Tests.Utils;
using TimeRunners.Mobile.Utils;
using Xunit;
using Record = TimeRunners.Mobile.Model.Record;

namespace TimeRunners.Mobile.Tests.Model
{
    public class UserTests
    {
        private User _user;

        public UserTests()
        {
            _user = new User();
        }

        [Fact]
        public void StartsANewRecord()
        {
            var record = _user.RecordRun();

            Assert.NotNull(record);
            Assert.Equal(_user, record.User);
        }

        [Fact]
        public void ContinuesARecordIfNotFinishedExists()
        {
            var item = new Record();
            
            _user.Records.Add(item);
            var record = _user.RecordRun();

            Assert.NotNull(record);
            Assert.Equal(item, record);
        }

        [Fact]
        public void ContinuesARecordIfPausedExists()
        {
            var item = new Record();
            item.Pause();
            _user.Records.Add(item);
            var record = _user.RecordRun();

            Assert.NotNull(record);
            Assert.Equal(item, record);
        }

        [Fact]
        public void StoresNewRecord()
        {
            var record = _user.RecordRun();

            Assert.NotNull(record);
            Assert.True(_user.Records.Contains(record));
        }

        [Fact]
        public void ContinuesRunWhenFinished()
        {
            var record = _user.RecordRun();
            record.Finish();

            _user.ContinueRun(record);

            Assert.Equal(RecordState.Recording, record.State);
        }

        [Fact]
        public void ContinuesPreviousRunWhenAnotherRunIsInProgress()
        {
            var previousRecord = _user.RecordRun();
            previousRecord.Finish();

            var currentRecord = _user.RecordRun();
            _user.ContinueRun(previousRecord);

            Assert.Equal(RecordState.Recording, previousRecord.State);
            Assert.Equal(RecordState.Finished, currentRecord.State);
        }
    }
}
