﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeRunners.Mobile.Utils;

namespace TimeRunners.Mobile.Tests.Utils
{
    public class ManualTimeProvider : TimeProvider
    {
        private DateTime _now;

        public void SetTime(DateTime time)
        {
            _now = time;
        }

        public override DateTime Now => _now;
    }
}
